package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(worldCmd)
}

var worldCmd = &cobra.Command{
	Use:   "world",
	Short: "Print world",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("world !")
	},
}
