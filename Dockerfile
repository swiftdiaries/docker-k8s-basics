FROM golang:1.15

# Add local package files in a similar fashion to the Go Module
ADD . /go/src/github.com/swiftdiaries/docker-k8s-basics

WORKDIR /go/src/github.com/swiftdiaries/docker-k8s-basics

RUN go mod download

# Build the CLI and save the output binary to /go/bin
RUN go build -o /go/bin/helloworld /go/src/github.com/swiftdiaries/docker-k8s-basics/main.go

CMD [ "/go/bin/helloworld" ]
