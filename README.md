# Docker Kubernetes 101

A 101 project where we walk through:

- [x] Building a Golang CLI
- [x] Containerizing it
- [x] Deploying it to Kubernetes

## Build a CLI app in Go

We use [Cobra](https://cobra.dev/) for bootstrapping a simple Go CLI App.

1. Create a `main.go`

```go
package main

import (
	"github.com/swiftdiaries/docker-k8s-basics/cmd"
)

func main() {
	cmd.Execute()
}
```

1. Create a `cmd` dir from the root of your repo

1. Create a `cmd/root.go`

```go
package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "helloworld",
	Short: "A simple helloworld CLI",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Hello !")
	},
}

// Execute is the cobra entrypoint for the CLI App
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
```

1. Create subcommands `cmd/subcommand1.go` and so on...

1. Create `cmd/world.go` 

```go
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(worldCmd)
}

var worldCmd = &cobra.Command{
	Use:   "world",
	Short: "Print world",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("world !")
	},
}
```

## Containerize the CLI app

1. Create a `Dockerfile` at the root of the repo

```
FROM golang:1.15

# Add local package files in a similar fashion to the Go Module
ADD . /go/src/github.com/swiftdiaries/docker-k8s-basics

WORKDIR /go/src/github.com/swiftdiaries/docker-k8s-basics

RUN go mod download

# Build the CLI and save the output binary to /go/bin
RUN go build -o /go/bin/helloworld /go/src/github.com/swiftdiaries/docker-k8s-basics/main.go

CMD [ "/go/bin/helloworld" ]
```

Note: We use `CMD` instead of `ENTRYPOINT` because we want to be able to call sub-commands.

1. Build the container

`docker build -f Dockerfile -t swiftdiaries/helloworld:v0.1 .`

1. Run the CLI from docker

`docker run swiftdiaries/helloworld:v0.1 helloworld --help`

The format is `docker run [IMAGE] COMMAND ARGS`

## Run the CLI App in Kubernetes

1. Create a toy-cluster with [Kind](https://kind.sigs.k8s.io/)

`kind create cluster`

1. Create a PodSpec for the CLI App `helloworld_pod.yaml`

```yaml
kind: Pod
apiVersion: v1
metadata:
  name: helloworld-cli
spec:
  containers:
    - name: helloworld-cli
      image: swiftdiaries/helloworld:v0.1
      command: ["helloworld", "--help"]
  restartPolicy: Never
```

1. Load the local image onto the k8s cluster

`kind load docker-image swiftdiaries/helloworld:v0.1`

1. Run the Pod

`kubectl create -f helloworld_pod.yaml`
