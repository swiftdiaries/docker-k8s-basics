package main

import (
	"github.com/swiftdiaries/docker-k8s-basics/cmd"
)

func main() {
	cmd.Execute()
}
